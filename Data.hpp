#ifndef DATA_H
#define DATA_H

#include<iostream>
#include <algorithm>
#include <vector>
#include<functional>

struct Data {
    std::uint8_t  x;
    double y;
 
 //For future use
    friend bool operator == (const Data & sp,const Data & ss){
        return (sp.x==ss.x and sp.y==ss.y);
    }
};
#endif

