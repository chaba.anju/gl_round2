#include <algorithm>
#include<iostream>
#include <vector>
#include <deque>
#include <list>
#include <forward_list>
#include <type_traits>

#include "Data.hpp"

using namespace std;

//TODO
// template<class Container>
// void reorder_container(Container& container, uint8_t n, uint8_t x_threshold,  std::function<void(Data&)> callback) {

//     if constexpr (std::is_same_v<std::remove_cv_t<Container>, std::array<typename Container::value_type, std::tuple_size_v<Container>>>)
//       reorder_sortArray(container,n,x_threshold,callback);
//     else
//      reorder_sortC(container,n,x_threshold,callback);
// }


template<class Container>
void reorder_sortC(Container& container, uint8_t n, uint8_t x_threshold,  std::function<void(Data&)> callback) {

    static_assert(std::is_same_v<typename Container::value_type, Data>, "T must be a container of Data");
    static_assert(std::is_same_v<decltype(x_threshold), uint8_t>, "x_threshold must be std::uint8_t");
    static_assert(std::is_same_v<decltype(n), uint8_t>, "n must be std::uint8_t");
    
      if(container.empty()){
        return;
    }
      // Partition the container into subsets A and B
    auto iter = std::stable_partition(container.begin(), container.end(), [x_threshold](const auto& element) {
        return element.x > x_threshold;
    });
    
    std::vector<Data> b_subset(iter, container.end());
    std::size_t n_elements = std::min(static_cast<std::vector<Data>::size_type>(n), b_subset.size());
    
    std::partial_sort(b_subset.begin(), b_subset.begin() + n_elements, b_subset.end(), [](const Data& a, const Data& b) {
        return a.y < b.y;
    });
    
     std::for_each(b_subset.begin(), b_subset.begin() + n_elements, callback);
    //For forward_list
    if constexpr (std::is_same_v<Container, std::forward_list<typename Container::value_type>>) {
        
        auto position=container.before_begin();
        while(std::next(position)!=iter)
        {
             position++;   
        }
        container.erase_after(position,container.end());
        container.insert_after(position,b_subset.begin(),b_subset.end());
        b_subset.clear();
    }
    else
    {
        container.erase(iter, container.end());
        std::move(b_subset.begin(),b_subset.end(),back_inserter(container));
    }
}

 template <typename Callback, std::size_t N>
 void reorder_sortArray(std::array<Data, N>& container, std::size_t n,std::uint8_t x_threshold,Callback callback) {
     
        if(container.empty()){
        return;
        }
    
        // Partition the array into two subsets: A (x > x_threshold) and B (x <= x_threshold)
        auto partition_it = std::stable_partition(container.begin(), container.end(), [&](const Data& elem) {
            return elem.x > x_threshold;
        });
        // Sort elements of subset B in ascending order of y
        std::size_t count = std::min(n, static_cast<size_t>(container.end()-partition_it));
      
       // auto ptr = partition_it + std::min(n, container.size() - std::distance(container.begin(),partition_it));
        std::partial_sort(partition_it,partition_it+count, container.end(), [](const Data& lhs, const Data& rhs) {
            return lhs.y < rhs.y;
        });
        // Apply callback to first n elements of subset B
        std::for_each(partition_it, partition_it+count, callback);
    } 