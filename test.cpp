//Problem statement
/*
    For application which operates on various sequence STL containers with elements of type:
    ```
    struct Data {
        std::uint8_t x;
        double y;
    };
    ```
    * Let container elements with x bigger than given threshold ```x_threshold``` be subset A.
    * Let container elements with x smaller or equal to given threshold ```x_threshold``` be subset B.

Implement library with function which:
1. Reorders the container in such a way, that subset A preceeds subset B.
2. Reorders elements of subset B in such a way, that given number ```n``` of elements has smallest value of y and this n elements are in ascending order of y. Let this ```n``` elements be subset C.
3. Accepts callback as a parameter and applies it to elements of subset C. Callback takes ```Data&``` as parameter and returns no result.

Non-functional requirements:
* Source code should be stored in gitlab (e.g. http://gitlab.com)
* Create unit tests for the library
* Run unit tests in gitlab CI

*/
#include<iostream>
#include<vector>
#include<list>
#include<deque>
#include <forward_list>
#include<array> 
#include<string>
#include<cassert>

#include "Data.hpp"
#include "Reorder_lib.hpp"

using namespace std;

void printResult(bool val, int count){
   if(val)
   cout<<"\n Reorder successful for test case : "<<count;
   else
   cout<<"\n Reorder unsuccessful for test case : "<<count;
}

template<class T>
bool testCallback(T smpl,std::function<void(Data&)> callback){
    
    T temp=smpl;
    for(auto &x:smpl){
        callback(x);
    }
    
    if(temp==smpl){
        return true;
    }
    return false;
}
template<class T>
void check_req123( T smpl, uint8_t n, uint8_t x_threshold,  std::function<void(Data&)> callback,int count) 
{
   //Calling method
     reorder_sortC(smpl, n, x_threshold, callback);
     //check requirement 1
 
     bool a_before_b = std::is_partitioned(smpl.begin(), smpl.end(), [x_threshold](const auto& element) {
        return element.x > x_threshold;
    }); 
  
    //checking requirement 2,3
       auto  pp = std::partition_point(smpl.cbegin(), smpl.cend(), [x_threshold](const auto& element) {
        return element.x > x_threshold;
    });
     std::vector<Data> b_subset;
    for(auto it=pp; it!=smpl.end();it++){
        b_subset.push_back(*it);
    }
  
    std::size_t n_elements = std::min(static_cast<std::vector<Data>::size_type>(n), b_subset.size());
    bool issorted = is_sorted(b_subset.begin(), b_subset.begin()+n_elements, [](const Data& a, const Data& b) {
       return a.y < b.y;});
     bool result = (a_before_b && issorted && testCallback(smpl,callback));
    printResult(result,count);
  
}

template<typename container>
void testContainers( auto callback,string contype) {

cout<<"\n testing container - "<<contype;
        // Corner case 1: empty container
   
    container container1{};
    check_req123(container1, 0, 0, callback,1);
    
    // Corner case 2: singleton container
    container container2{{1, 1.0}};
    check_req123(container2, 0, 0, callback,2);
   
    // Corner case 3: all elements in subset A
    container container3{{5, 1.0}, {6, 2.0}, {7, 3.0}};
    check_req123(container3, 2, 8, callback,3);
    
    // Corner case 4: all elements in subset B
    container container4{{1, 3.0}, {2, 2.0}, {3, 1.0}};
    check_req123(container4, 0, 4, callback,4);
    
    // Corner case 5: subset A is empty
    container container5{{1, 1.0}, {2, 2.0}, {3, 3.0}};
    check_req123(container5, 0, 3, callback,5);
    
    // Corner case 6: subset B is empty
    container container6{{5, 1.0}, {6, 2.0}, {7, 3.0}};
    check_req123(container6, 0, 8, callback,6);
    
    // Corner case 7: n is equal to the size of subset C
    container container8{{1, 1.0}, {2, 2.0}, {3, 3.0}};
    check_req123(container8, 0, 3, callback,7);
    
    // Corner case 8: n is larger than the size of subset C
    container container9{{1, 1.0}, {2, 2.0}, {3, 3.0}};
    check_req123(container9, 0, 4, callback,8);
    
    // Corner case 9: n is smaller than or equal to 0
    container container10{{1, 1.0}, {2, 2.0}, {3, 3.0}};
    check_req123(container10, 0, -1, callback,9);

}
    

void testArrayCases(auto callback) {

cout<<"\n Testing array";
// Corner case 1: empty container
std::array<Data,0> container1;
testArray(container1, 0, 0, callback,1);

// Corner case 2: singleton container
std::array<Data,1> container2{{1, 1.0}};
testArray(container2, 0, 0, callback,2);

// Corner case 3: all elements in subset A
std::array<Data,3> container3{{{5, 1.0}, {6, 2.0}, {7, 3.0}}};
testArray(container3, 2, 8, callback,3);


// Corner case 4: all elements in subset B
std::array<Data,3> container4{{{1, 3.0}, {2, 2.0}, {3, 1.0}}};
testArray(container4, 0, 4, callback,4);


// Corner case 4: subset A is empty
std::array<Data,3> container5{{{1, 1.0}, {2, 2.0}, {3, 3.0}}};
testArray(container5, 0, 3, callback,4);

// Corner case 5: subset B is empty
std::array<Data,3> container6{{{5, 1.0}, {6, 2.0}, {7, 3.0}}};
testArray(container6, 0, 8, callback,5);


// Corner case 6 n is equal to the size of subset C
std::array<Data,3> container8{{{1, 1.0}, {2, 2.0}, {3, 3.0}}};
testArray(container8, 0, 3, callback,6);


// Corner case 7: n is larger than the size of subset C
std::array<Data,3> container9{{{1, 1.0}, {2, 2.0}, {3, 3.0}}};
testArray(container9, 0, 4, callback,7);

// Corner case 8: n is smaller than or equal to 0
std::array<Data,3> container10{{{1, 1.0}, {2, 2.0}, {3, 3.0}}};
testArray(container10, 0, -1, callback,8);

}

template <typename T, std::size_t N>
void testArray(std::array<T, N> smpl, uint8_t n, uint8_t x_threshold,  std::function<void(Data&)> callback,int no)
{
    reorder_sortArray(smpl, n, x_threshold, callback);
    bool a_before_b = std::is_partitioned(smpl.begin(), smpl.end(), [x_threshold](const auto& element) {
        return element.x > x_threshold;
    }); 
    
    auto  partition_it = std::partition_point(smpl.cbegin(), smpl.cend(), [x_threshold](const auto& element) {
        return element.x > x_threshold;
    });
       
    std::size_t count = std::min(n, static_cast<uint8_t>(smpl.end()-partition_it));
    bool issorted = is_sorted(partition_it,partition_it+count, [](const Data& a, const Data& b) {
      return a.y < b.y;});
     bool result= (a_before_b && issorted &&  testCallback(smpl,callback));
    printResult(result, no);
         
}

int main()
{
    uint8_t x_threshold = 3,n=2;

    auto callback = [](Data& data) {
            //do something
        };
        
    testContainers<std::vector<Data>>(callback,"vector");
    testContainers<std::deque<Data>>(callback,"deque");
    testContainers<std::list<Data>>(callback,"list");
    testContainers<std::forward_list<Data>>(callback,"forward_list");
    testArrayCases(callback);

    return 0;
}


